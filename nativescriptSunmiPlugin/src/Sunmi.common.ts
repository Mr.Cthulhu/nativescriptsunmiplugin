import { Observable } from 'tns-core-modules/data/observable';

export class Common {
    public printer: com.sunmi.sunmiv2.services.SunmiPrinter;
    constructor(ctx) {
        this.initPrinter(ctx);
    }

    public initPrinter(context: globalAndroid.content.Context): com.sunmi.sunmiv2.services.SunmiPrinter {
      try {
             this.printer = com.sunmi.sunmiv2.services.SunmiPrinter.getInstance();
             this.printer.initPrinter(context);
        } catch (err) {
          console.log(err);
        }
        return this.printer;
    }
    public setTextBold(bold: boolean = true): void {
        this.printer.runRAWData([27, 69, bold ? 1 : 0], null);
    }

    public setInvertedWhiteBlack(inverted: boolean = true): void {
        this.printer.runRAWData([29, 66, inverted ? 1 : 0], null);
    }
}