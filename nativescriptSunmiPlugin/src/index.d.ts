/// <reference path="./typings/sunmi.d.ts" />

export declare class Sunmi {
  constructor(ctx)
  public printerSunmi: com.sunmi.sunmiv2.services.SunmiPrinter;
  public setTextBold(mode: boolean): void;
  public setInvertedWhiteBlack(mode: boolean): void;
}
