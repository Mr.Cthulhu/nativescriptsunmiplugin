/// <reference path="android-declarations.d.ts"/>

declare module com {
	export module sunmi {
		export module sunmiv2 {
			export class BuildConfig {
				public static class: java.lang.Class<com.sunmi.sunmiv2.BuildConfig>;
				public static DEBUG: boolean;
				public static LIBRARY_PACKAGE_NAME: string;
				public static APPLICATION_ID: string;
				public static BUILD_TYPE: string;
				public static FLAVOR: string;
				public static VERSION_CODE: number;
				public static VERSION_NAME: string;
				public constructor();
			}
		}
	}
}

declare module com {
	export module sunmi {
		export module sunmiv2 {
			export module contracts {
				export class SunmiCallback {
					public static class: java.lang.Class<com.sunmi.sunmiv2.contracts.SunmiCallback>;
					/**
					 * Constructs a new instance of the com.sunmi.sunmiv2.contracts.SunmiCallback interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
					 */
					public constructor(implementation: {
						onRunResult(param0: boolean): void;
						onReturnString(param0: string): void;
						onRaiseException(param0: number, param1: string): void;
					});
					public constructor();
					public onRaiseException(param0: number, param1: string): void;
					public onReturnString(param0: string): void;
					public onRunResult(param0: boolean): void;
				}
			}
		}
	}
}

declare module com {
	export module sunmi {
		export module sunmiv2 {
			export module services {
				export class SunmiPrinter {
					public static class: java.lang.Class<com.sunmi.sunmiv2.services.SunmiPrinter>;
					public getFirmwareStatus(): number;
					public commitPrinterBuffer(param0: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public static getInstance(): com.sunmi.sunmiv2.services.SunmiPrinter;
					public runRAWData(param0: native.Array<number>, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public printColumnsText(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public printBarCode(param0: string, param1: number, param2: number, param3: number, param4: number, param5: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public printQRCode(param0: string, param1: number, param2: number, param3: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public exitPrinterBuffer(param0: boolean, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public printText(param0: string, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public setFontName(param0: string, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public getServiceVersion(): string;
					public sendRAWData(param0: native.Array<number>, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public getPrinterModal(): string;
					public setFontSize(param0: number, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public getPrinterVersion(): string;
					public printTextWithFont(param0: string, param1: string, param2: number, param3: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public printerInit(param0: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public setAlignment(param0: number, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public getPrinterSerialNo(): string;
					public printBitmapCustom(param0: globalAndroid.graphics.Bitmap, param1: number, param2: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public initPrinter(param0: globalAndroid.content.Context): void;
					public printOriginalText(param0: string, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public printerSelfChecking(param0: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public getPrintedLength(param0: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public printBitmap(param0: globalAndroid.graphics.Bitmap, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public enterPrinterBuffer(param0: boolean, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
					public lineWrap(param0: number, param1: com.sunmi.sunmiv2.contracts.SunmiCallback): void;
				}
			}
		}
	}
}

declare module woyou {
	export module aidlservice {
		export module jiuiv5 {
			export class ICallback {
				public static class: java.lang.Class<woyou.aidlservice.jiuiv5.ICallback>;
				/**
				 * Constructs a new instance of the woyou.aidlservice.jiuiv5.ICallback interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
				 */
				public constructor(implementation: {
					onRunResult(param0: boolean): void;
					onReturnString(param0: string): void;
					onRaiseException(param0: number, param1: string): void;
				});
				public constructor();
				public onReturnString(param0: string): void;
				public onRunResult(param0: boolean): void;
				public onRaiseException(param0: number, param1: string): void;
			}
			export module ICallback {
				export class Default extends woyou.aidlservice.jiuiv5.ICallback {
					public static class: java.lang.Class<woyou.aidlservice.jiuiv5.ICallback.Default>;
					public constructor();
					public onRaiseException(param0: number, param1: string): void;
					public onReturnString(param0: string): void;
					public asBinder(): globalAndroid.os.IBinder;
					public onRunResult(param0: boolean): void;
				}
				export abstract class Stub implements woyou.aidlservice.jiuiv5.ICallback {
					public static class: java.lang.Class<woyou.aidlservice.jiuiv5.ICallback.Stub>;
					public constructor();
					public onRaiseException(param0: number, param1: string): void;
					public static setDefaultImpl(param0: woyou.aidlservice.jiuiv5.ICallback): boolean;
					public static getDefaultImpl(): woyou.aidlservice.jiuiv5.ICallback;
					public onTransact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
					public onReturnString(param0: string): void;
					public asBinder(): globalAndroid.os.IBinder;
					public onRunResult(param0: boolean): void;
					public static asInterface(param0: globalAndroid.os.IBinder): woyou.aidlservice.jiuiv5.ICallback;
				}
				export module Stub {
					export class Proxy extends woyou.aidlservice.jiuiv5.ICallback {
						public static class: java.lang.Class<woyou.aidlservice.jiuiv5.ICallback.Stub.Proxy>;
						public static sDefaultImpl: woyou.aidlservice.jiuiv5.ICallback;
						public getInterfaceDescriptor(): string;
						public onRunResult(param0: boolean): void;
						public onRaiseException(param0: number, param1: string): void;
						public asBinder(): globalAndroid.os.IBinder;
						public onReturnString(param0: string): void;
					}
				}
			}
		}
	}
}

declare module woyou {
	export module aidlservice {
		export module jiuiv5 {
			export class IWoyouService {
				public static class: java.lang.Class<woyou.aidlservice.jiuiv5.IWoyouService>;
				/**
				 * Constructs a new instance of the woyou.aidlservice.jiuiv5.IWoyouService interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
				 */
				public constructor(implementation: {
					postPrintData(param0: string, param1: native.Array<number>, param2: number, param3: number): boolean;
					getFirmwareStatus(): number;
					getServiceVersion(): string;
					printerInit(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					printerSelfChecking(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					getPrinterSerialNo(): string;
					getPrinterVersion(): string;
					getPrinterModal(): string;
					getPrintedLength(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					lineWrap(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					sendRAWData(param0: native.Array<number>, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					setAlignment(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					setFontName(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					setFontSize(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					printText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					printTextWithFont(param0: string, param1: string, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					printColumnsText(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					printBitmap(param0: globalAndroid.graphics.Bitmap, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					printBarCode(param0: string, param1: number, param2: number, param3: number, param4: number, param5: woyou.aidlservice.jiuiv5.ICallback): void;
					printQRCode(param0: string, param1: number, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					printOriginalText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					commitPrinterBuffer(): void;
					enterPrinterBuffer(param0: boolean): void;
					exitPrinterBuffer(param0: boolean): void;
					printColumnsString(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					printBitmapCustom(param0: globalAndroid.graphics.Bitmap, param1: number, param2: woyou.aidlservice.jiuiv5.ICallback): void;
					getForcedDouble(): number;
					isForcedAntiWhite(): boolean;
					isForcedBold(): boolean;
					isForcedUnderline(): boolean;
					getForcedRowHeight(): number;
					getFontName(): number;
				});
				public constructor();
				public getPrinterSerialNo(): string;
				public lineWrap(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				public isForcedUnderline(): boolean;
				public postPrintData(param0: string, param1: native.Array<number>, param2: number, param3: number): boolean;
				public sendRAWData(param0: native.Array<number>, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				public printQRCode(param0: string, param1: number, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
				public printColumnsString(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
				public printBarCode(param0: string, param1: number, param2: number, param3: number, param4: number, param5: woyou.aidlservice.jiuiv5.ICallback): void;
				public enterPrinterBuffer(param0: boolean): void;
				public getForcedDouble(): number;
				public exitPrinterBuffer(param0: boolean): void;
				public setFontName(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				public printText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				public printColumnsText(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
				public getFontName(): number;
				public printTextWithFont(param0: string, param1: string, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
				public isForcedAntiWhite(): boolean;
				public setFontSize(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				public printerInit(param0: woyou.aidlservice.jiuiv5.ICallback): void;
				public isForcedBold(): boolean;
				public getPrinterVersion(): string;
				public printBitmap(param0: globalAndroid.graphics.Bitmap, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				public printerSelfChecking(param0: woyou.aidlservice.jiuiv5.ICallback): void;
				public printOriginalText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				public getPrinterModal(): string;
				public getServiceVersion(): string;
				public commitPrinterBuffer(): void;
				public getFirmwareStatus(): number;
				public getPrintedLength(param0: woyou.aidlservice.jiuiv5.ICallback): void;
				public setAlignment(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				public printBitmapCustom(param0: globalAndroid.graphics.Bitmap, param1: number, param2: woyou.aidlservice.jiuiv5.ICallback): void;
				public getForcedRowHeight(): number;
			}
			export module IWoyouService {
				export class Default extends woyou.aidlservice.jiuiv5.IWoyouService {
					public static class: java.lang.Class<woyou.aidlservice.jiuiv5.IWoyouService.Default>;
					public postPrintData(param0: string, param1: native.Array<number>, param2: number, param3: number): boolean;
					public setFontSize(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public getFirmwareStatus(): number;
					public getFontName(): number;
					public setAlignment(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public printQRCode(param0: string, param1: number, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					public enterPrinterBuffer(param0: boolean): void;
					public printColumnsText(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					public exitPrinterBuffer(param0: boolean): void;
					public commitPrinterBuffer(): void;
					public sendRAWData(param0: native.Array<number>, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public printBitmap(param0: globalAndroid.graphics.Bitmap, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public getForcedRowHeight(): number;
					public constructor();
					public isForcedAntiWhite(): boolean;
					public printerInit(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					public printTextWithFont(param0: string, param1: string, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					public isForcedBold(): boolean;
					public getServiceVersion(): string;
					public getPrinterModal(): string;
					public printOriginalText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public getPrinterVersion(): string;
					public getForcedDouble(): number;
					public setFontName(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public printColumnsString(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					public asBinder(): globalAndroid.os.IBinder;
					public getPrinterSerialNo(): string;
					public printText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public getPrintedLength(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					public printBarCode(param0: string, param1: number, param2: number, param3: number, param4: number, param5: woyou.aidlservice.jiuiv5.ICallback): void;
					public printerSelfChecking(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					public isForcedUnderline(): boolean;
					public printBitmapCustom(param0: globalAndroid.graphics.Bitmap, param1: number, param2: woyou.aidlservice.jiuiv5.ICallback): void;
					public lineWrap(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				}
				export abstract class Stub implements woyou.aidlservice.jiuiv5.IWoyouService {
					public static class: java.lang.Class<woyou.aidlservice.jiuiv5.IWoyouService.Stub>;
					public postPrintData(param0: string, param1: native.Array<number>, param2: number, param3: number): boolean;
					public setFontSize(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public getFirmwareStatus(): number;
					public getFontName(): number;
					public setAlignment(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public printQRCode(param0: string, param1: number, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					public onTransact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
					public static asInterface(param0: globalAndroid.os.IBinder): woyou.aidlservice.jiuiv5.IWoyouService;
					public enterPrinterBuffer(param0: boolean): void;
					public printColumnsText(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					public exitPrinterBuffer(param0: boolean): void;
					public commitPrinterBuffer(): void;
					public sendRAWData(param0: native.Array<number>, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public printBitmap(param0: globalAndroid.graphics.Bitmap, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public getForcedRowHeight(): number;
					public constructor();
					public isForcedAntiWhite(): boolean;
					public printerInit(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					public printTextWithFont(param0: string, param1: string, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					public isForcedBold(): boolean;
					public getServiceVersion(): string;
					public static getDefaultImpl(): woyou.aidlservice.jiuiv5.IWoyouService;
					public getPrinterModal(): string;
					public printOriginalText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public getPrinterVersion(): string;
					public getForcedDouble(): number;
					public static setDefaultImpl(param0: woyou.aidlservice.jiuiv5.IWoyouService): boolean;
					public setFontName(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public printColumnsString(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
					public asBinder(): globalAndroid.os.IBinder;
					public getPrinterSerialNo(): string;
					public printText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
					public getPrintedLength(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					public printBarCode(param0: string, param1: number, param2: number, param3: number, param4: number, param5: woyou.aidlservice.jiuiv5.ICallback): void;
					public printerSelfChecking(param0: woyou.aidlservice.jiuiv5.ICallback): void;
					public isForcedUnderline(): boolean;
					public printBitmapCustom(param0: globalAndroid.graphics.Bitmap, param1: number, param2: woyou.aidlservice.jiuiv5.ICallback): void;
					public lineWrap(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
				}
				export module Stub {
					export class Proxy extends woyou.aidlservice.jiuiv5.IWoyouService {
						public static class: java.lang.Class<woyou.aidlservice.jiuiv5.IWoyouService.Stub.Proxy>;
						public static sDefaultImpl: woyou.aidlservice.jiuiv5.IWoyouService;
						public printColumnsText(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
						public getServiceVersion(): string;
						public printColumnsString(param0: native.Array<string>, param1: native.Array<number>, param2: native.Array<number>, param3: woyou.aidlservice.jiuiv5.ICallback): void;
						public printOriginalText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
						public getForcedRowHeight(): number;
						public printBitmapCustom(param0: globalAndroid.graphics.Bitmap, param1: number, param2: woyou.aidlservice.jiuiv5.ICallback): void;
						public getInterfaceDescriptor(): string;
						public isForcedUnderline(): boolean;
						public printBitmap(param0: globalAndroid.graphics.Bitmap, param1: woyou.aidlservice.jiuiv5.ICallback): void;
						public getPrintedLength(param0: woyou.aidlservice.jiuiv5.ICallback): void;
						public printTextWithFont(param0: string, param1: string, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
						public asBinder(): globalAndroid.os.IBinder;
						public setFontSize(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
						public enterPrinterBuffer(param0: boolean): void;
						public postPrintData(param0: string, param1: native.Array<number>, param2: number, param3: number): boolean;
						public getPrinterModal(): string;
						public printerSelfChecking(param0: woyou.aidlservice.jiuiv5.ICallback): void;
						public printQRCode(param0: string, param1: number, param2: number, param3: woyou.aidlservice.jiuiv5.ICallback): void;
						public getForcedDouble(): number;
						public commitPrinterBuffer(): void;
						public isForcedBold(): boolean;
						public getFirmwareStatus(): number;
						public printerInit(param0: woyou.aidlservice.jiuiv5.ICallback): void;
						public lineWrap(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
						public sendRAWData(param0: native.Array<number>, param1: woyou.aidlservice.jiuiv5.ICallback): void;
						public printText(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
						public isForcedAntiWhite(): boolean;
						public getPrinterSerialNo(): string;
						public setFontName(param0: string, param1: woyou.aidlservice.jiuiv5.ICallback): void;
						public setAlignment(param0: number, param1: woyou.aidlservice.jiuiv5.ICallback): void;
						public printBarCode(param0: string, param1: number, param2: number, param3: number, param4: number, param5: woyou.aidlservice.jiuiv5.ICallback): void;
						public getFontName(): number;
						public getPrinterVersion(): string;
						public exitPrinterBuffer(param0: boolean): void;
					}
				}
			}
		}
	}
}

//Generics information:

