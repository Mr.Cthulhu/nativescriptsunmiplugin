import { Common } from './Sunmi.common';

export class Sunmi extends Common {
    public printerSunmi: com.sunmi.sunmiv2.services.SunmiPrinter;

    constructor(ctx) {
        super(ctx);
        this.printerSunmi = this.initPrinter(ctx);
    }
    public setTextBold(bold: boolean = true): void {
        this.setTextBold(bold);
    }
    public setInvertedWhiteBlack(inverted: boolean = true): void {
        this.setInvertedWhiteBlack(inverted);
    }
}
